#!/usr/bin/env node

import * as assetgraphIpfs from "assetgraph-ipfs";
import AssetGraph from "assetgraph";
import assetGraphRollup from "assetgraph-rollup";
import crypto from "crypto";
import glob from "glob";
import postcss from "postcss";
import uncss from "uncss";
import util from "util";
import { program } from "commander";

process.exitCode = 1;

if ("@runtime_PATH@" != "@runtime" + "_PATH@")
	process.env.PATH = "@runtime_PATH@";

program.version("0.0.0");
program.arguments("<source> [dest]");
program.option("--asset-mode <mode>", "How to move assets. ipfs|hash|none");
program.option("--deployed-url <prefix>", "The root of the deployed site");
program.option("--link-mode <mode>", "Rewrite links to this style. relative|rootRelative|absolute");
program.option("--no-js-optimize");
program.parse();
const ARGS = program.opts();

const TRUE = () => true;
const URL_ROOT = import.meta.url.replace(/[^\/]*$/, "");
const PATH_ROOT = URL_ROOT.slice("file://".length);

async function addMonetization(html) {
	html.forEach(a => {
			a.parseTree.head.insertAdjacentHTML("beforeend", "<meta name=monetization content=$ilp.uphold.com/7hzaJdm7KBZp>");
			a.markDirty();
		});
}

async function addReporting(html) {
	html.forEach(a => {
			a.parseTree.head.insertAdjacentHTML("beforeend", [
				`<meta http-equiv=report-to content='{"max_age":31536000,"endpoints":[{"url":"https://c.report-uri.com/a/d/g"}],"include_subdomains":true}'>`,
				`<meta http-equiv=nel content='{"report_to":"default","max_age":31536000,"include_subdomains":true}'>`,
			].join(""))
			a.markDirty();
		});
}

async function addWebmention(html) {
	html.forEach(a => {
			a.parseTree.head.insertAdjacentHTML("beforeend", [
				"<link rel=webmention href=https://webmention.io/kevincox.ca/webmention>",
				"<link rel=pingback href=https://webmention.io/kevincox.ca/xmlrpc>",
			].join(""))
			a.markDirty();
		});
}

async function compileCss(g) {
	await g.autoprefixer();
	await Promise.all(
		g.findRelations({
			type: "HtmlStyle",
			to: {isInline: true},
		}).map(async r => {
			if (!("prune" in r.node.dataset)) return;
			delete r.node.dataset.prune;

			let [html, css] = await Promise.all([r.from.load(), r.to.load()]);
			let cleaned = await postcss([
					uncss.postcssPlugin({
						html: html.text,
						ignoreSheets: [/./],
						jsdom: {
							features: null,

							// Would rather set `undefined` but this isn't possible: https://github.com/uncss/uncss/issues/511.
							runScripts: "outside-only",
						},
					})
				])
				.process(css.parseTree, {from: undefined});

			r.to = css.clone()
			r.to.parseTree = cleaned.root;
		}));
}

async function compileJs(g) {
	g.javaScriptSerializationOptions = {
		compact: true,
		comment: false,
	};

	if (!ARGS.jsOptimize) return;

	await Promise.all(
		g.findAssets({type: "Html", protocol: "file:"})
			.map(a =>
				assetGraphRollup(
					g,
					a,
					{configFileName: PATH_ROOT + "rollup.config.js"})));

	await g.compressJavaScript(
		{$or: [
			{protocol: "file:"},
			{isInline: true, incomingInlineRelation: {type: "HtmlScript"}},
		]},
		"terser",
		{
			module: true,
			mangleOptions: {
				toplevel: true,
			},
		});
}

async function configureFeed(g) {
	await g.convertStylesheetsToInlineStyles(
		{
			incomingInlineRelation: {
				from: { type: "Atom" },
			},
		},
		"screen,prefers-color-scheme: dark");

	// Use absolute URLs in feed HTML.
	g.findRelations({ from: { type: "Atom" }, to: { isInline: true } })
		.forEach(r => r.to.outgoingRelations
			.forEach(r => r.canonical = true));

	g.findAssets({ type: "Atom", protocol: "file:" })
		.forEach(a => {
			for (let link of a.parseTree.getElementsByTagName("id")) {
				let href = g.resolveUrl(g.root, link.textContent);
				href = g.buildHref(href, null, {canonical: true});
				link.textContent = href;
			}
			for (let link of a.parseTree.getElementsByTagName("link")) {
				let href = link.getAttribute("href");
				href = g.resolveUrl(g.root, href);
				href = g.buildHref(href, null, {canonical: true});
				link.setAttribute("href", href);
			}
			a.markDirty();
		});
}

async function hashAssets(assets) {
	let dependencies = {};
	for (let a of assets) {
		let r = dependencies[a.url] = {};
		r.promise = new Promise((resolve, _reject) => {
			r.resolve = resolve;
			// r.reject = reject; // We will raise an exception.
		});
	}

	await Promise.all(
		assets.map(async a => {
			a = await a.load();
			let url = a.url;

			for (let r of a.outgoingRelations) {
				if (r.to.url == url) {
					// This works as long as the asset doesn't depend on its own URL.
					continue;
				}

				let dep = dependencies[r.to.url];
				if (!dep) { continue }

				await dep.promise;
			}

			let digest = crypto.createHash("sha256")
				.update(a.rawSrc)
				.digest("base64url")
				.slice(0, 6);

			let name = a.fileName;
			if (name.startsWith("asset-dir-prefix-")) {
				name = name
					.replace(/^asset-dir-prefix-/, "")
					.replace(/\.[0-9a-f]{8}\./, ".");
			}

			a.url = `/a/${digest}/${name}`;

			dependencies[url].resolve();
		}));
}

async function main(root, output="dist") {
	let g = new AssetGraph({
		root,
	});
	g.on("info", console.info);
	g.followRelations = {
		crossorigin: false,
		type: {$nin: [
			"SourceMapFile",
			"SourceMapSource",
		]},
	};

	g.canonicalRoot = ARGS.deployedUrl;

	// All files including hidden files.
	await Promise.all((await util.promisify(glob)(`${root}/**/*`, {dot: true, nodir: true}))
		.map(async path => {
			let a = g.addAsset(`file://${encodeURI(path)}`);

			// Wrong type inference can corrupt assets. Fix up cases that hit us.
			if (a.extension === ".opml") {
				a.contentType = "text/x-opml";
			}

			await a.load();
		}));

	await g.populate();

	for (let r of g.findRelations({to: {isPopulated: false}})) {
		r.remove();
	}

	function isEntry(a) {
		return a.url.startsWith(g.root)
			&& !a.path.startsWith("/a/")
			&& !(a.path == "/" && a.fileName && a.fileName.startsWith("asset-dir-prefix-"));
	}

	function getAll(extra=TRUE) {
		return g.findAssets({
			protocol: "file:",
			$where: extra,
		});
	}
	function getEntries(extra=TRUE) {
		return getAll(a => isEntry(a) && extra(a));
	}
	function getAssets(extra=TRUE) {
		return getAll(a => !isEntry(a) && extra(a));
	}

	let htmlMinifierOptions = {
		collapseWhitespace: false,
		removeOptionalTags: true,
	};
	g.findAssets({type: "Html", protocol: "file:"})
		.forEach(a => {
			a.htmlMinifierOptions = htmlMinifierOptions;
		});

	let html = getEntries(a => a.type == "Html");
	await addMonetization(html);
	await addReporting(html);
	await addWebmention(html);
	await configureFeed(g);

	g.findRelations({
		to: {protocol: "file:", query: {inline: ""}},
	}).forEach(r => {
		if (r.type === "HtmlPreloadLink") {
			// If we are going to inline the asset we don't want to preload it.
			r.detach();
			return;
		}

		r.inline()
	});

	await Promise.all([
		compileCss(g),
		compileJs(g),
	]);

	// NOTE: This must come after JavaScript compilation otherwise the reference will be lost when the code is reparsed.
	g.findRelations({type: "JavaScriptStaticUrl"})
		.map(a => a.omitFunctionCall());

	if (ARGS.linkMode) {
		g.findRelations({
				from: {protocol: "file:", $where: a => isEntry(a)},
				to: {protocol: "file:", $where: a => isEntry(a)},
			})
			.forEach(r => r.hrefType = ARGS.linkMode);
	}

	await Promise.all(g.findAssets({isLoaded: true}).map(a => a.minify && a.minify()));

	g.findRelations({ from: { type: "Atom" }, to: { type: "Html" } })
		.forEach(r => {
			if (r.node.childNodes.length != 1) {
				console.log("Warning, more than one child for feed HTML, skipping.", r.node.childNodes);
				return;
			}

			let content = r.node.firstChild;
			if (content.nodeName != "#text") {
				console.log("Feed HTML is not a text node", content);
				return;
			}

			r.node.textContent = "";
			r.node.append(r.node.ownerDocument.createCDATASection(r.to.text));
			r.from.markDirty();
		});

	switch (ARGS.assetMode) {
		case undefined:
		case "ipfs":
			await assetgraphIpfs.useGateway(getAssets(), {
				gatewayUrl({asset, cid, path}) {
					let gateway = "cloudflare-ipfs.com"
					if (asset.contentType.startsWith("video/")) {
						gateway = "ipfs.io";
					}
					return `https://${gateway}/ipfs/${cid.toV0()}/${path}`;
				},
			});
			break;
		case "hash":
			await hashAssets(getAssets());
			break;
		case "none":
			break;
		default:
			throw new Error(`Unknown --asset-mode=${JSON.stringify(ARGS.asset_mode)}`);
	}

	await g.writeAssetsToDisc({protocol: "file:"}, output)
		.writeStatsToStderr();
}

main(...program.args).then(() => process.exitCode = 0, console.error);
