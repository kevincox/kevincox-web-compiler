{
	nixpkgs ? import <nixpkgs> {},
	napalm ? nixpkgs.pkgs.callPackage (
		fetchTarball "https://github.com/nix-community/napalm/archive/master.tar.gz"
	) {},
}:
with nixpkgs; rec {
	bin = napalm.buildPackage
		(pkgs.nix-gitignore.gitignoreSource [
			"*.nix"
			".*"
		] ./.)
		{
			runtime_PATH = lib.makeBinPath (with pkgs; [
				file
			]);
			patchPhase = ''
				substituteInPlace main.js --subst-var runtime_PATH
			'';
		};

	fixtures = lib.mapAttrs
		(name: {args ? [], src}:
			pkgs.runCommand
				"kevincox-web-compiler-fixture-${name}-www"
				{
					buildInputs = [bin];
				}
				("echo $out; " + (lib.escapeShellArgs
					([
						"kevincox-web-compiler"
					] ++ args ++ [
						"--"
						"${src}"
					]) + " $out")))
		{
			one = {
				src = fixtures/1;
			};
			hash = {
				src = fixtures/1;
				args = [
					"--asset-mode=hash"
				];
			};
			top-level-await = {
				src = fixtures/top-level-await;
			};
		};

	all-www = pkgs.runCommand "kevincox-web-compiler-all-www" {} ''
		mkdir $out
		${lib.concatStrings
			(lib.mapAttrsToList
				(name: path: "ln -vs ${path} $out/${name}\n")
				fixtures)}
	'';
}
