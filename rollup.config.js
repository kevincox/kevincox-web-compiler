export default {
	external: /^https:/,
	output: {
		chunkFileNames: "a/rollup-[name]-[hash]/[name].js",
	}
};
